from flask import Flask, request
from celery import Celery

app = Flask(__name__)

app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'  # Use Redis as the message broker
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'  # Use Redis as the result backend

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

@app.route('/process_request', methods=['POST'])
def process_request():
    data = request.get_json()
    if data:
        # Add the task to the Celery queue
        task = add_to_celery_queue.delay(data)
        return 'Task {} added to the Celery queue.'.format(task.id)
    else:
        return 'Invalid data provided', 400
    
@app.route('/get_result/<task_id>', methods=['GET'])
def get_task_result(task_id):
    result = add_to_celery_queue.AsyncResult(task_id)
    return result.get()

@celery.task
def add_to_celery_queue(data):
    result = 'hello {}'.format(data.get('name'))
    print('Task result: {}'.format(result))
    return result

if __name__ == '__main__':
    app.run(debug=True, port=8001)

